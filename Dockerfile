FROM node:12
  
WORKDIR /usr/src/app

# copy any local packages service depends on
COPY sdk/arrayhelper ./sdk/arrayhelper

# copy the service itself
COPY services/api ./services/api

# copy root level config / manifests
COPY lerna.json .
COPY package.json .
COPY package-lock.json .

# install dependencies and add symlinks
RUN npm install
RUN npm run bootstrap

# start api
CMD cd services/api && npm run start

EXPOSE 3000


