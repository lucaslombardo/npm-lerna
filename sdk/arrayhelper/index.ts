import * as lodash from "lodash";

const arrayhelper = {
  lodash,
  addOne: (arr: string[]) => [...arr, 1]
};

export default arrayhelper;
