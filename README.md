# running / dockerizing
In root directory terminal:

1. npm run bootstrap
1. npm run build
1. docker build -t npm-lerna .
1. docker run -p 3000:3000 npm-lerna