import * as Koa from "koa";
import arrayhelper from "@sdk/arrayhelper";

const app = new Koa();

app.use(async ctx => {
  ctx.body = `
    ${JSON.stringify(arrayhelper.addOne(["hi", "bye"]))}\n
    ${JSON.stringify(arrayhelper.lodash.flatten([1, [2]]))}
  `;
});

app.listen(3000);
